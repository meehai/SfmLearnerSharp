import matplotlib.pyplot as plt
import numpy as np
import cv2
from matplotlib.cm import hot
from media_processing_lib.video import tryReadVideo
from media_processing_lib.image import toImage, imgResize
from neural_wrappers.utilities import minMax
from tqdm import trange

def Hot(x):
	x = hot(minMax(x))[..., 0 : 3]
	return x

def inference_video(model, videoPath, outPath="res.mp4"):
    video = tryReadVideo(videoPath, vidLib="pims")
    N = len(video)

    height, width = imgResize(video[0], height=240, width=None, interpolation="bilinear").shape[0 : 2]
    out = cv2.VideoWriter(outPath, cv2.VideoWriter_fourcc(*'mp4v'), video.fps, (width * 2, height))
    for i in trange(N, desc="[inference_video]"):
        frame = video[i]
        frame = imgResize(frame, height=240, width=None, interpolation="bilinear")
        npImg = (((np.array(frame, dtype=np.float32) / 255) - 0.5) * 2).transpose(2, 0, 1)[None]
        npDisp = model.dispNet.npForward(npImg)[0][0]
        disp = toImage(Hot(npDisp))
 
        res = np.concatenate([frame, disp], axis=1)
        out.write(res[..., ::-1])
    out.release()