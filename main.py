import yaml
import torch.optim as optim
import numpy as np
import torch as tr
from argparse import ArgumentParser
from neural_wrappers.utilities import getGenerators, npGetInfo, changeDirectory
from neural_wrappers.callbacks import SaveModels, PlotMetrics, SaveHistory
from neural_wrappers.pytorch import device

from models import getModel
from readers import getReader
# from reader import SfmLearnerVideoDirReader, mergeFn
from utils import RandomPlotEpoch
from plot_dataset import plot_dataset
from inference_video import inference_video

np.random.seed(42)
tr.manual_seed(42)

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("dataCfg")

	parser.add_argument("--modelCfg")

	parser.add_argument("--dir")
	parser.add_argument("--weightsFile")
	parser.add_argument("--batchSize", type=int, default=10)
	parser.add_argument("--numEpochs", type=int, default=100)
	args = parser.parse_args()
	if not args.modelCfg is None:
		args.modelCfg = yaml.load(open(args.modelCfg, "r"), Loader=yaml.Loader)

	if args.type != "inference_video":
		args.dataCfg = yaml.load(open(args.dataCfg, "r"), Loader=yaml.Loader)
		args.dataCfg["sequenceSize"] = 1 if args.type in ("plot_dataset", "test") \
			else args.modelCfg["SfmLearner"]["sequenceSize"]
	return args

def main():
	args = getArgs()
	reader = getReader(args.dataCfg)
	print(reader)

	if args.type == "plot_dataset":
		plot_dataset(reader.iterate())
		exit()

	model = getModel(args.modelCfg).to(device)
	model.addCallbacks([SaveModels("best", "Loss"), SaveModels("last", "Loss"), SaveHistory("history.txt")])
	model.setOptimizer(optim.Adam, lr=0.001)
	print(model.summary())

	if args.type == "inference_video":
		model.loadModel(args.weightsFile)
		model.eval()
		inference_video(model, args.dataCfg)
		exit()

	generator = reader.iterate()
	model.addCallback(RandomPlotEpoch(len(generator)))

	if args.type == "train":
		changeDirectory(args.dir, expectExist=False)
		model.trainGenerator(generator, args.numEpochs)
	elif args.type == "retrain":
		model.loadModel(args.weightsFile)
		changeDirectory(args.dir, expectExist=True)
		model.trainGenerator(generator, args.numEpochs)
	elif args.type == "test":
		model.loadModel(args.weightsFile)
		# model.saveModel(args.weightsFile)
		# exit()
		res = model.testGenerator(generator, printMessage="v2")
		print(res)

if __name__ == "__main__":
	main()