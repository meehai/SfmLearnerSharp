import matplotlib.pyplot as plt
from media_processing_lib.image import toImage

def plot_dataset(generator):
    sequenceSize = generator.reader.sequenceSize
    for (data, labels), B in generator:
        print(data["sourceRgbs"].mean(), data["targetRgb"].mean())
        for j in range(B):
            ax = plt.subplots(1, sequenceSize + 1)[1]
            targetRgb = data["targetRgb"][j]
            sourceRgbs = data["sourceRgbs"][j]

            ax[0].imshow(toImage(targetRgb))
            for k in range(sequenceSize):
                ax[k + 1].imshow(toImage(sourceRgbs[k]))
            plt.show()
            plt.close()
