import numpy as np
from simple_caching import NpyFS 
from neural_wrappers.readers import MergeBatchedDatasetReader, StaticBatchedDatasetReader, CachedDatasetReader, \
	RandomIndexDatasetReader

def getReader(readerCfg):
	if isinstance(readerCfg, str):
		return readerCfg

	if readerCfg["type"] == "VideoDirReader":
		from .video_dir_reader import SfmLearnerVideoDirReader, mergeFn
		K = np.load(readerCfg["intrinsicsPath"])
		reader = SfmLearnerVideoDirReader(readerCfg["datasetPath"], K, readerCfg["sequenceSize"])
		reader = CachedDatasetReader(reader, cache=NpyFS("%s/.cache/sfmlearner/seq%d" % \
			(readerCfg["datasetPath"], readerCfg["sequenceSize"])))
		reader = RandomIndexDatasetReader(reader, seed=42)
		reader = MergeBatchedDatasetReader(reader, mergeFn=mergeFn)
		reader = StaticBatchedDatasetReader(reader, readerCfg["batchSize"])
		return reader

	if readerCfg["type"] == "Carla":
		from .carla_reader import SfmLearnerCarlaReader
		reader = SfmLearnerCarlaReader(readerCfg["datasetPath"], \
			sequenceSize=readerCfg["sequenceSize"], hyperParameters=readerCfg["hyperParameters"])
		reader = StaticBatchedDatasetReader(reader, readerCfg["batchSize"])
		return reader

	assert False
