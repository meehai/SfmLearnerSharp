import torch as tr
from neural_wrappers.pytorch import NWModule
from neural_wrappers.pytorch.models import UNetTinySum

class DispNetTinySum(UNetTinySum):
    def __init__(self, numFilters):
        super().__init__(dIn=3, dOut=1, numFilters=numFilters)

    def forward(self, x):
        yFinal = super().forward(x)
        y = tr.sigmoid(yFinal)
        return y