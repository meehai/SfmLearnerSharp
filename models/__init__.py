def getDispNetModel(modelCfg):
    if modelCfg["type"] == "DispNetTinySum":
        from .dispnet import DispNetTinySum
        dispNet = DispNetTinySum(numFilters=modelCfg["numFilters"])
    return dispNet

def getRelativePoseNet(modelCfg):
    if modelCfg["type"] == "SfmLearnerRelativePoseNet":
        from .relative_pose import SfmLearnerRelativePoseNet
        relativePoseNet = SfmLearnerRelativePoseNet()
    return relativePoseNet

def getModel(modelCfg):
    if modelCfg["SfmLearner"]["type"] == "v1":
        from .v1 import SfmLearnerV1

        assert "DispNet" in modelCfg
        assert "RelativePoseNet" in modelCfg
        dispNet = getDispNetModel(modelCfg["DispNet"])
        relativePoseNet = getRelativePoseNet(modelCfg["RelativePoseNet"])
        sfmLearnerModel = SfmLearnerV1(dispNet=dispNet, relativePoseNet=relativePoseNet, hyperParameters=modelCfg)
    elif modelCfg["SfmLearner"]["type"] == "paper":
        from .paper import SfmLearnerPaper
        sfmLearnerModel = SfmLearnerPaper(hyperParameters=modelCfg)

    return sfmLearnerModel