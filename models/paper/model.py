from overrides import overrides
from .DispNetS import DispNetS
from .PoseExpNet import PoseExpNet
from .loss_functions import photometric_reconstruction_loss, explainability_loss, smooth_loss
from ..sfm_learner import SfmLearner

class SfmLearnerPaper(SfmLearner):
	def __init__(self, **k):
		# NWModule.__init__(self, **k)
		dispNet = DispNetS()
		params = k["hyperParameters"]["SfmLearner"]
		relativePoseNet = PoseExpNet(nb_ref_imgs=params["sequenceSize"], output_exp=params["maskWeight"] > 0)
		super().__init__(dispNet, relativePoseNet, **k["hyperParameters"])

	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining, isOptimizing):
		# From https://github.com/ClementPinard/SfmLearner-Pytorch/blob/master/train.py
		intrinsics = trInputs["intrinsics"]
		tgt_img = trInputs["targetRgb"]
		ref_imgs = trInputs["sourceRgbs"].permute(1, 0, 2, 3, 4)
		disp_net = self.dispNet
		pose_exp_net = self.relativePoseNet
		w1 = self.hyperParameters["SfmLearner"]["photometricWeight"]
		w2 = self.hyperParameters["SfmLearner"]["maskWeight"]
		w3 = self.hyperParameters["SfmLearner"]["smoothWeight"]

		# compute output
		depth = disp_net(tgt_img)
		if not isTraining:
			assert len(ref_imgs) == 1
			ref_imgs = ref_imgs.repeat(pose_exp_net.nb_ref_imgs, 1, 1, 1, 1)
		explainability_mask, pose = pose_exp_net(tgt_img, ref_imgs)

		loss_1, warped, diff = photometric_reconstruction_loss(tgt_img, ref_imgs, intrinsics,
																depth, explainability_mask, pose)
		if w2 > 0:
			loss_2 = explainability_loss(explainability_mask)
		else:
			loss_2 = 0
		loss_3 = smooth_loss(depth)

		trLoss = w1*loss_1 + w2*loss_2 + w3*loss_3
		self.updateOptimizer(trLoss, isTraining, isOptimizing)

		trResults = {
			"sourceRgbs" : trLabels["sourceRgbs"], "targetRgb" : trLabels["targetRgb"], \
			"ySourceToTargetWarped" : warped[0][0].unsqueeze(dim=1), "yTargetDisp" : depth, \
			"warpMask" : diff[0][0], "photometricLoss" : loss_1.detach()
		}

		return trResults, trLoss
