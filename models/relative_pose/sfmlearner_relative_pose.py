import torch as tr
import torch.nn as nn

# Generic network that computes "correlation features" between two RGB frames, concatenating them by the last channel
class CorrelateTwoFrames(nn.Module):
	@staticmethod
	def conv(in_planes, out_planes, kernel_size=3):
		return nn.Sequential(
			nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, padding=(kernel_size-1)//2, stride=2),
			nn.ReLU(inplace=True)
		)

	def __init__(self):
		super(CorrelateTwoFrames, self).__init__()

		conv_planes = [16, 32, 64, 128, 256, 256, 256]
		self.conv1 = CorrelateTwoFrames.conv(in_planes=6, out_planes=conv_planes[0], kernel_size=7)
		self.conv2 = CorrelateTwoFrames.conv(in_planes=conv_planes[0], out_planes=conv_planes[1], kernel_size=5)
		self.conv3 = CorrelateTwoFrames.conv(in_planes=conv_planes[1], out_planes=conv_planes[2], kernel_size=3)
		self.conv4 = CorrelateTwoFrames.conv(in_planes=conv_planes[2], out_planes=conv_planes[3], kernel_size=3)
		self.conv5 = CorrelateTwoFrames.conv(in_planes=conv_planes[3], out_planes=conv_planes[4], kernel_size=3)
		self.conv6 = CorrelateTwoFrames.conv(in_planes=conv_planes[4], out_planes=conv_planes[5], kernel_size=3)
		self.conv7 = CorrelateTwoFrames.conv(in_planes=conv_planes[5], out_planes=conv_planes[6], kernel_size=3)

	def forward(self, x1, x2):
		x = tr.cat([x1, x2], dim=1)
		out_conv1 = self.conv1(x)
		out_conv2 = self.conv2(out_conv1)
		out_conv3 = self.conv3(out_conv2)
		out_conv4 = self.conv4(out_conv3)
		out_conv5 = self.conv5(out_conv4)
		out_conv6 = self.conv6(out_conv5)
		out_conv7 = self.conv7(out_conv6)
		return out_conv7

class SfmLearnerRelativePoseNet(nn.Module):
	def __init__(self):
		super(SfmLearnerRelativePoseNet, self).__init__()
		self.correlateTwoFrames = CorrelateTwoFrames()
		self.poseFinalConv = nn.Conv2d(256, 6, kernel_size=1, padding=0)

	def forward(self, x1, x2):
		yTwoFrames = self.correlateTwoFrames(x1, x2)
		yPose = self.poseFinalConv(yTwoFrames)
		yPose = yPose.mean(dim=3).mean(dim=2)
		# Magic numbers I need to figure out....
		yPose = yPose * 0.01
		yRelativeTranslation, yRelativeRotation = yPose[:, 0 : 3], yPose[:, 3 : ]
		return {"yRelativeTranslation" : yRelativeTranslation, "yRelativeRotation" : yRelativeRotation}
